package com.fsquiroz.deployer.exception;

import org.springframework.http.HttpStatus;

public abstract class AppException extends Exception {

    protected AppException(String message) {
        super(message);
    }

    protected AppException(Throwable cause) {
        super(cause);
    }

    protected AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public abstract String getUserMessage();

    public abstract String getErrorMessage();

    public abstract HttpStatus getStatus();

}
