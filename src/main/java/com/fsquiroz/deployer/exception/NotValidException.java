package com.fsquiroz.deployer.exception;

import java.util.Map;
import org.springframework.http.HttpStatus;

public class NotValidException extends AppException {

    private final Map<String, String> params;

    public NotValidException(String message) {
        this(message, null);
    }

    public NotValidException(String message, Map<String, String> params) {
        super(message);
        this.params = params;
    }

    public NotValidException(Throwable cause, String message) {
        super(message, cause);
        this.params = null;
    }

    @Override
    public String getUserMessage() {
        return super.getMessage();
    }

    @Override
    public String getErrorMessage() {
        if (params == null || params.isEmpty()) {
            return getMessage();
        }
        StringBuilder sb = new StringBuilder(getMessage());
        sb.append(" params(");
        boolean first = true;
        for (String key : params.keySet()) {
            if (!first) {
                sb.append("&");
            } else {
                first = false;
            }
            sb.append(key);
            sb.append("=");
            sb.append(params.get(key));
        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

}
