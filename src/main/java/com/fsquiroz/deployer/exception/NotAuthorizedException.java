package com.fsquiroz.deployer.exception;

import com.fsquiroz.deployer.entity.db.AccessToken;
import org.springframework.http.HttpStatus;

public class NotAuthorizedException extends AppException {

    private final String token;

    private final AccessToken accessToken;

    public NotAuthorizedException(String message, AccessToken accessToken) {
        super(message);
        this.accessToken = accessToken;
        token = null;
    }

    public NotAuthorizedException(String message, String token) {
        super(message);
        this.accessToken = null;
        this.token = token;
    }

    @Override
    public String getUserMessage() {
        return super.getMessage();
    }

    @Override
    public String getErrorMessage() {
        if (accessToken == null && token == null) {
            return getMessage();
        }
        StringBuilder sb = new StringBuilder(getMessage());
        if (accessToken != null) {
            sb.append(" (User '");
            sb.append(accessToken.getNickname());
            sb.append("')");
        } else if (token != null) {
            sb.append(" (Token '");
            sb.append(token);
            sb.append("')");
        }
        return sb.toString();
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.FORBIDDEN;
    }

}
