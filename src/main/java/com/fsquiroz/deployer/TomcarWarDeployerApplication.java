package com.fsquiroz.deployer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TomcarWarDeployerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TomcarWarDeployerApplication.class, args);
    }

}
