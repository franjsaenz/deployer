package com.fsquiroz.deployer.config;

import com.fsquiroz.deployer.entity.db.Project;
import java.io.File;
import org.springframework.stereotype.Service;

@Service
public class AppConfig {

    private File appHome;

    public AppConfig() {
        appHome = new File(getBasePath());
    }

    public File getAppHome() {
        return new File(appHome.toURI());
    }

    public File getProjects() {
        return new File(getFilePath("projects.json"));
    }

    public File getTokens() {
        return new File(getFilePath("tokens.json"));
    }

    public File getProjectFolder(Project p) {
        return new File(getFilePath(p.getId()));
    }

    public File getProjectPom(Project p) {
        StringBuilder sb = new StringBuilder(getFilePath(p.getId()));
        sb.append(File.separator);
        sb.append("pom.xml");
        return new File(sb.toString());
    }

    public File getProjectBuild(Project p) {
        StringBuilder sb = new StringBuilder(getFilePath(p.getId()));
        sb.append(File.separator);
        sb.append(p.getPackagePath());
        return new File(sb.toString());
    }

    public File getProjectTarget(Project p) {
        return new File(p.getContextPath());
    }

    private String getBasePath() {
        String userHome = System.getProperty("user.home");
        StringBuilder sb = new StringBuilder(userHome);
        sb.append(File.separator);
        sb.append(".deployer");
        return sb.toString();
    }

    private String getFilePath(String fileName) {
        StringBuilder sb = new StringBuilder(getBasePath());
        sb.append(File.separator);
        sb.append(fileName);
        return sb.toString();
    }

}
