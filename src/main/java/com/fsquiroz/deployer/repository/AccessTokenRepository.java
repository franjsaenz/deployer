package com.fsquiroz.deployer.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccessTokenRepository {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private ObjectMapper mapper;

    private Map<String, AccessToken> getDB() throws AppException {
        try {
            File db = appConfig.getTokens();
            if (db.exists()) {
                Map<String, AccessToken> tokens = mapper.readValue(db, new TypeReference<HashMap<String, AccessToken>>() {
                });
                return tokens;
            } else {
                return new HashMap<>();
            }
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to open db file");
        }
    }

    private void saveDB(Map<String, AccessToken> tokens) throws AppException {
        try {
            File db = appConfig.getTokens();
            mapper.writeValue(db, tokens);
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to save db file");
        }
    }

    public void save(AccessToken at) throws AppException {
        Map<String, AccessToken> tokens = getDB();
        String id;
        if (at.getId() == null) {
            id = Long.toString(System.currentTimeMillis(), 16);
            at.setId(id);
        } else {
            id = at.getId();
        }
        tokens.put(id, at);
        saveDB(tokens);
    }

    public AccessToken get(String id) throws AppException {
        Map<String, AccessToken> tokens = getDB();
        return tokens.get(id);
    }

    public void delete(AccessToken at) throws AppException {
        Map<String, AccessToken> tokens = getDB();
        tokens.remove(at.getId());
        saveDB(tokens);
    }

    public List<AccessToken> getAll() throws AppException {
        Map<String, AccessToken> tokens = getDB();
        return new ArrayList<>(tokens.values());
    }

}
