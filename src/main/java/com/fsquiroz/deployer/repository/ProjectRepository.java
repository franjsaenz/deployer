package com.fsquiroz.deployer.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectRepository {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private ObjectMapper mapper;

    private Map<String, Project> getDB() throws AppException {
        try {
            File db = appConfig.getProjects();
            if (db.exists()) {
                Map<String, Project> proyects = mapper.readValue(db, new TypeReference<HashMap<String, Project>>() {
                });
                return proyects;
            } else {
                return new HashMap<>();
            }
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to open db file");
        }
    }

    private void saveDB(Map<String, Project> projects) throws AppException {
        try {
            File db = appConfig.getProjects();
            mapper.writeValue(db, projects);
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to save db file");
        }
    }

    public void save(Project p) throws AppException {
        Map<String, Project> db = getDB();
        String id;
        if (p.getId() == null) {
            id = Long.toString(System.currentTimeMillis(), 16);
            p.setId(id);
        } else {
            id = p.getId();
        }
        db.put(id, p);
        saveDB(db);
    }

    public Project get(String id) throws AppException {
        Map<String, Project> db = getDB();
        return db.get(id);
    }

    public void delete(Project p) throws AppException {
        Map<String, Project> db = getDB();
        db.remove(p.getId());
        saveDB(db);
    }

    public List<Project> getAll() throws AppException {
        List<Project> projects = getAllWithDel();
        List<Project> result = new ArrayList<>();
        for (Project p : projects) {
            if (p.getDeleted() == null) {
                result.add(p);
            }
        }
        return result;
    }

    public List<Project> getAllWithDel() throws AppException {
        Map<String, Project> db = getDB();
        return new ArrayList<>(db.values());
    }

}
