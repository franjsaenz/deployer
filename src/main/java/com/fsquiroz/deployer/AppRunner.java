package com.fsquiroz.deployer;

import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.entity.json.MAccessToken;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.service.AccessTokenService;
import java.io.File;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    private Logger log = LoggerFactory.getLogger(AppRunner.class);

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private AccessTokenService accessTokenService;

    @Override
    public void run(ApplicationArguments aa) throws Exception {
        log.info("Checking app folder");
        File f = appConfig.getAppHome();
        if (!f.exists()) {
            log.info("Creating app folder '{}'", f.getPath());
            f.mkdir();
        }
        valLastAdminToken();
    }

    private void valLastAdminToken() throws AppException {
        log.info("Checking last admin token");
        AccessToken at = accessTokenService.lastAdminToken();
        if (at == null) {
            MAccessToken mat = new MAccessToken();
            mat.setAdmin(true);
            mat.setCreated(new Date());
            mat.setNickname("admin");
            at = accessTokenService.create(mat);
        }
        log.info("Current admin access token:\n{}", at.getToken());
    }

}
