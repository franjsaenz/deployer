package com.fsquiroz.deployer.entity.json;

public class MResponse {

    private String status;

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static MResponse ok(String message) {
        MResponse mr = new MResponse();
        mr.status = "ok";
        mr.message = message;
        return mr;
    }
    
    public static MResponse err(String message) {
        MResponse mr = new MResponse();
        mr.status = "error";
        mr.message = message;
        return mr;
    }
}
