package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import com.fsquiroz.deployer.repository.ProjectRepository;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectExecutor implements Runnable {

    private Logger log = LoggerFactory.getLogger(ProjectExecutor.class);

    private ProjectRepository projectRepository;

    private AppConfig appConfig;

    private GitService gitService;

    private MavenService mavenService;

    private Project project;

    private boolean success;

    private String errorMessage;

    private long init;

    public ProjectExecutor(ProjectRepository projectRepository, AppConfig appConfig,
            GitService gitService, MavenService mavenService, Project project) {
        this.projectRepository = projectRepository;
        this.appConfig = appConfig;
        this.gitService = gitService;
        this.mavenService = mavenService;
        this.project = project;
        success = false;
    }

    @Override
    public void run() {
        init = System.currentTimeMillis();
        try {
            startProcess();
            updateProject();
            mavenService.clean(project);
            mavenService.build(project);
            deploy();
            success = true;
        } catch (AppException e) {
            log.error(e.getErrorMessage(), e);
            getError(e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            getError(e);
        }
        endProcess();
    }

    private void startProcess() throws AppException {
        log.info("Starting process for Project '{}' id '{}'", project.getName(), project.getId());
        project.setProcessRunning(true);
        projectRepository.save(project);
    }

    private void updateProject() throws AppException {
        gitService.update(project);
    }

    private void deploy() throws AppException {
        File build = appConfig.getProjectBuild(project);
        File target = appConfig.getProjectTarget(project);
        if (!build.exists()) {
            throw new NotValidException("Build file can not beThere was an error while attempint to deploy");
        }
        log.info("Copying '{}' into '{}'", build.getAbsolutePath(), target.getAbsolutePath());
        try {
            Files.copy(build.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new NotValidException(e, "There was an error while attempting to deploy");
        }
    }

    private void endProcess() {
        project.setProcessRunning(false);
        project.setLastUpdateSuccess(success);
        project.setLastErrorMessage(errorMessage);
        if (success) {
            project.setLastSuccess(new Date());
        } else {
            project.setLastError(new Date());
        }
        try {
            projectRepository.save(project);
        } catch (AppException e) {
            log.error(e.getErrorMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        long spend = System.currentTimeMillis() - init;
        log.info("Proccess end for Project '{}' id '{}'", project.getName(), project.getId());
        log.info("Proccess took {} ms", spend);
    }

    private void getError(AppException e) {
        this.errorMessage = e.getErrorMessage();
        if (e.getCause() != null) {
            getCause(e.getCause());
        }
    }

    private void getError(Throwable t) {
        this.errorMessage = t.getMessage();
        if (t.getCause() != null) {
            getCause(t.getCause());
        }
    }

    private void getCause(Throwable t) {
        this.errorMessage += ". CAUSE: ";
        this.errorMessage += t.getMessage();
        if (t.getCause() != null) {
            getCause(t.getCause());
        }
    }

}
