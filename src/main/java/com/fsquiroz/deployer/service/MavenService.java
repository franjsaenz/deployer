package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MavenService {

    private Logger log = LoggerFactory.getLogger(MavenService.class);

    @Autowired
    private AppConfig appConfig;

    public void validatePom(Project p) throws AppException {
        File pom = appConfig.getProjectPom(p);
        if (!pom.exists()) {
            Map<String, String> params = new HashMap<>();
            params.put("pom", pom.getAbsolutePath());
            throw new NotValidException("Project does not have pom.xml file", params);
        }
    }

    public void clean(Project p) throws AppException {
        log.info("Performing clean on project '{}' id '{}'", p.getName(), p.getId());
        execute(p, Arrays.asList("clean"));
        log.info("Clean proccess completed");
    }

    public void build(Project p) throws AppException {
        log.info("Performing package on project '{}' id '{}'", p.getName(), p.getId());
        execute(p, Arrays.asList("package"));
        log.info("Package proccess completed");
    }

    private void execute(Project p, List<String> goals) throws AppException {
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(appConfig.getProjectPom(p));
        request.setGoals(goals);
        Invoker invoker = new DefaultInvoker();
        request.setBatchMode(true);
        invoker.setMavenHome(new File(p.getMavenHome()));
        try {
            InvocationResult result = invoker.execute(request);
            if (result.getExitCode() != 0) {
                throw new NotValidException("Maven proccess exited with code " + result.getExitCode());
            }
        } catch (MavenInvocationException e) {
            throw new NotValidException(e, "Maven proccess could not be executed");
        }
    }

}
