package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.entity.json.MAccessToken;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotAuthorizedException;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.repository.AccessTokenRepository;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessTokenService {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    public String createToken() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[64];
        random.nextBytes(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }

    public AccessToken create(MAccessToken mat) throws AppException {
        mat.setId(null);
        mat.setLastUsed(null);
        AccessToken at = new AccessToken();
        at.setAdmin(mat.isAdmin());
        at.setCreated(new Date());
        at.setDeleted(null);
        at.setExpire(mat.getExpire());
        at.setLastUsed(null);
        at.setNickname(mat.getNickname());
        at.setToken(createToken());
        accessTokenRepository.save(at);
        return at;
    }

    public AccessToken getAdminToken(String token) throws AppException {
        AccessToken at = getByToken(token);
        if (!at.isAdmin()) {
            throw new NotAuthorizedException("Access token not authorized to perform this action", at);
        }
        return at;
    }

    public AccessToken getByToken(String token) throws AppException {
        Date now = new Date();
        AccessToken at = null;
        List<AccessToken> tokens = accessTokenRepository.getAll();
        for (AccessToken t : tokens) {
            if (token.equals(t.getToken())) {
                at = t;
                break;
            }
        }
        if (at == null) {
            throw new NotFoundException(token);
        }
        if ((at.getExpire() != null && at.getExpire().before(now)) || at.getDeleted() != null) {
            throw new NotAuthorizedException("Token expired", at);
        }
        at.setLastUsed(now);
        accessTokenRepository.save(at);
        return at;
    }

    public AccessToken getById(String id) throws AppException {
        AccessToken at = accessTokenRepository.get(id);
        if (at == null) {
            throw new NotFoundException(id);
        }
        return at;
    }

    public List<AccessToken> getAll() throws AppException {
        return accessTokenRepository.getAll();
    }

    public List<AccessToken> getAllNotRemoved() throws AppException {
        Date now = new Date();
        List<AccessToken> tokens = getAll();
        List<AccessToken> result = new ArrayList<>();
        for (AccessToken token : tokens) {
            if (token.getDeleted() == null
                    && !(token.getExpire() != null && token.getExpire().after(now))) {
                result.add(token);
            }
        }
        return result;
    }

    public void delete(AccessToken at) throws AppException {
        at.setDeleted(new Date());
        accessTokenRepository.save(at);
    }

    public AccessToken lastAdminToken() throws AppException {
        Date now = new Date();
        AccessToken at = null;
        List<AccessToken> tokens = accessTokenRepository.getAll();
        for (AccessToken token : tokens) {
            if (token.getDeleted() != null || token.getToken() == null
                    || !token.isAdmin()
                    || (token.getExpire() != null && token.getExpire().after(now))) {
                continue;
            }
            if (at == null || at.getCreated().before(token.getCreated())) {
                at = token;
            }
        }
        return at;
    }

    public MAccessToken db2json(AccessToken at) {
        MAccessToken mat = new MAccessToken();
        mat.setCreated(at.getCreated());
        mat.setExpire(at.getExpire());
        mat.setId(at.getId());
        mat.setLastUsed(at.getLastUsed());
        mat.setNickname(at.getNickname());
        mat.setAdmin(at.isAdmin());
        return mat;
    }

}
