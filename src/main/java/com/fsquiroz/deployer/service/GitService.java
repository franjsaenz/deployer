package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import java.io.File;
import javax.annotation.PreDestroy;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.lib.internal.WorkQueue;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GitService {

    private Logger log = LoggerFactory.getLogger(GitService.class);

    @Autowired
    private AppConfig appConfig;

    public void clone(Project p) throws AppException {
        log.info("Cloning project '{}' id '{}'", p.getName(), p.getId());
        File repo = appConfig.getProjectFolder(p);
        CloneCommand cmd = Git.cloneRepository()
                .setDirectory(repo)
                .setURI(p.getHttpUrl())
                .setBranch(p.getBranch())
                .setCredentialsProvider(getCredential(p));
        try {
            Git git = cmd.call();
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to clone repo");
        }
        log.info("Clones ended");
    }

    public void update(Project p) throws AppException {
        log.info("Upating project '{}' id '{}'", p.getName(), p.getId());
        checkout(p);
        reset(p);
        pull(p);
        log.info("Update ended");
    }

    public void checkout(Project p) throws AppException {
        File repo = appConfig.getProjectFolder(p);
        try {
            Git git = Git.open(repo);
            git.checkout()
                    .setName(p.getBranch())
                    .call();
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to checkout a branch");
        }
    }

    public void reset(Project p) throws AppException {
        File repo = appConfig.getProjectFolder(p);
        try {
            Git git = Git.open(repo);
            git.reset()
                    .setMode(ResetCommand.ResetType.HARD)
                    .call();
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to reset repo");
        }
    }

    public void pull(Project p) throws AppException {
        log.info("Pulling project '{}' id '{}'", p.getName(), p.getId());
        File repo = appConfig.getProjectFolder(p);
        try {
            Git git = Git.open(repo);
            PullResult ps = git.pull()
                    .setRemoteBranchName(p.getBranch())
                    .setCredentialsProvider(getCredential(p))
                    .call();
        } catch (Exception e) {
            throw new NotValidException(e, "Error while attempting to pull from origin");
        }
        log.info("Pull ended");
    }

    private CredentialsProvider getCredential(Project p) {
        return new UsernamePasswordCredentialsProvider("token", p.getAuthToken());
    }

    @PreDestroy
    public void destory() {
        WorkQueue.getExecutor().shutdownNow();
    }

}
