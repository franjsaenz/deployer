package com.fsquiroz.deployer.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.config.AppConfig;
import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.json.MAccessToken;
import com.fsquiroz.deployer.entity.json.MProject;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotAuthorizedException;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.exception.NotValidException;
import com.fsquiroz.deployer.repository.ProjectRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private AccessTokenService accessTokenService;

    @Autowired
    private GitService gitService;

    @Autowired
    private MavenService mavenService;

    public Project create(MProject mp) throws AppException {
        AccessToken at = accessTokenService.getById(mp.getAccessToken().getId());
        Project p = new Project();
        p.setAccessTokenId(at.getId());
        p.setAuthToken(mp.getAuthToken());
        p.setMavenHome(mp.getMavenHome());
        p.setBranch(mp.getBranch());
        p.setContextPath(mp.getContextPath());
        p.setCreated(new Date());
        p.setHttpUrl(mp.getHttpUrl());
        p.setName(mp.getName());
        p.setPackagePath(mp.getPackagePath());
        projectRepository.save(p);
        gitService.clone(p);
        p.setLastUpdateSuccess(true);
        projectRepository.save(p);
        return p;
    }

    public Project get(String id) throws AppException {
        Project p = projectRepository.get(id);
        if (p == null) {
            throw new NotFoundException(id);
        }
        return p;
    }

    public void edit(Project original, MProject edition) throws AppException {
        AccessToken at = accessTokenService.getById(edition.getAccessToken().getId());
        original.setAccessTokenId(at.getId());
        original.setAuthToken(edition.getAuthToken());
        original.setMavenHome(edition.getMavenHome());
        original.setBranch(edition.getBranch());
        original.setContextPath(edition.getContextPath());
        original.setHttpUrl(edition.getHttpUrl());
        original.setName(edition.getName());
        original.setPackagePath(edition.getPackagePath());
        projectRepository.save(original);
    }

    public void delete(Project p) throws AppException {
        p.setDeleted(new Date());
        projectRepository.save(p);
    }

    public List<Project> getAll() throws AppException {
        return projectRepository.getAll();
    }

    public void update(AccessToken at, Project p, JsonNode json) throws AppException {
        if (!at.getId().equals(p.getAccessTokenId())) {
            if (!at.isAdmin()) {
                throw new NotAuthorizedException("Access token not authorized to perform this action", at);
            }
        }
        if (json != null) {
            if (json.has("object_kind")) {
                String kind = json.get("object_kind").asText();
                if (!kind.equals("push")) {
                    throw new NotValidException("Event '" + kind + "' not supported");
                }
            }
            if (json.has("ref")) {
                String ref = json.get("ref").asText();
                if (!ref.endsWith(p.getBranch())) {
                    throw new NotValidException("Branch '" + ref + "' not supported");
                }
            }
        }
        new Thread(new ProjectExecutor(projectRepository, appConfig, gitService,
                mavenService, p)).start();
    }

    public MProject db2json(Project p) throws AppException {
        AccessToken at = accessTokenService.getById(p.getAccessTokenId());
        MAccessToken mat = accessTokenService.db2json(at);
        MProject mp = new MProject();
        mp.setAccessToken(mat);
        mp.setBranch(p.getBranch());
        mp.setMavenHome(p.getMavenHome());
        mp.setContextPath(p.getContextPath());
        mp.setCreated(p.getCreated());
        mp.setDeleted(p.getDeleted());
        mp.setHttpUrl(p.getHttpUrl());
        mp.setId(p.getId());
        mp.setLastUpdateSuccess(p.isLastUpdateSuccess());
        mp.setLastErrorMessage(p.getLastErrorMessage());
        mp.setLastError(p.getLastError());
        mp.setLastSuccess(p.getLastSuccess());
        mp.setName(p.getName());
        mp.setPackagePath(p.getPackagePath());
        mp.setProcessRunning(p.isProcessRunning());
        return mp;
    }

}
