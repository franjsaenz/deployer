package com.fsquiroz.deployer.controller.api;

import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.entity.json.MAccessToken;
import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.service.AccessTokenService;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AccessTokenController {

    private Logger log = LoggerFactory.getLogger(AccessTokenController.class);

    @Autowired
    private AccessTokenService accessTokenService;

    @GetMapping("/admin/token")
    public MResponse print() throws AppException {
        AccessToken at = accessTokenService.lastAdminToken();
        log.info("Current admin access token:\n{}", at.getToken());
        return MResponse.ok("Last admin access token printed in app console");
    }

    @GetMapping("/tokens")
    public List<MAccessToken> listAll(@RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        List<AccessToken> all = accessTokenService.getAllNotRemoved();
        List<MAccessToken> mat = new ArrayList<>();
        for (AccessToken at : all) {
            mat.add(accessTokenService.db2json(at));
        }
        return mat;
    }

    @PostMapping("/token")
    public MAccessToken create(@RequestHeader String token,
            @RequestBody MAccessToken mat) throws AppException {
        accessTokenService.getAdminToken(token);
        AccessToken at = accessTokenService.create(mat);
        MAccessToken c = accessTokenService.db2json(at);
        c.setToken(at.getToken());
        return c;
    }

    @GetMapping("/token/{tokenId}")
    public MAccessToken get(@PathVariable String tokenId,
            @RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        AccessToken at = accessTokenService.getById(tokenId);
        return accessTokenService.db2json(at);
    }

    @GetMapping("/token/{tokenId}/print")
    public MResponse print(@PathVariable String tokenId,
            @RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        AccessToken at = accessTokenService.getById(tokenId);
        log.info("Access token '{}':\n{}", at.getNickname(), at.getToken());
        return MResponse.ok("Access token for '" + at.getNickname() + "' printed in app console");
    }

    @DeleteMapping("/token/{tokenId}")
    public MResponse del(@PathVariable String tokenId,
            @RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        AccessToken at = accessTokenService.getById(tokenId);
        accessTokenService.delete(at);
        return MResponse.ok("Access token removed");
    }

}
