package com.fsquiroz.deployer.controller.api;

import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.exception.AppException;
import javax.servlet.http.HttpServletRequest;
import org.apache.catalina.connector.ClientAbortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice(annotations = RestController.class)
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(AppException.class)
    public ResponseEntity<MResponse> pogo(HttpServletRequest req, AppException ae) {
        if (ae.getCause() != null) {
            log.error(ae.getErrorMessage(), ae);
        } else {
            log.debug(ae.getErrorMessage(), ae);
        }
        log.info("AppException: {}", ae.getErrorMessage());
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.info("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.info("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        return new ResponseEntity(MResponse.err(ae.getErrorMessage()), ae.getStatus());
    }

    @ExceptionHandler(ClientAbortException.class)
    public void exception(HttpServletRequest req, ClientAbortException ex) {
        log.warn(ex.getMessage());
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.info("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.info("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<MResponse> gral(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.info("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.info("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        return new ResponseEntity(MResponse.err(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
