package com.fsquiroz.deployer.controller.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.entity.db.AccessToken;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.json.MProject;
import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.exception.NotValidException;
import com.fsquiroz.deployer.service.AccessTokenService;
import com.fsquiroz.deployer.service.ProjectService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private AccessTokenService accessTokenService;

    @GetMapping("/projects")
    public List<MProject> getAll(@RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        List<Project> projects = projectService.getAll();
        List<MProject> result = new ArrayList<>();
        for (Project p : projects) {
            result.add(projectService.db2json(p));
        }
        return result;
    }

    @GetMapping("/project/{projectId}")
    public MProject get(@PathVariable String projectId,
            @RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        Project p = projectService.get(projectId);
        return projectService.db2json(p);
    }

    @PostMapping("/project/{projectId}/update")
    public MResponse update(@PathVariable String projectId,
            @RequestHeader(required = false) String token,
            @RequestHeader(required = false, value = "X-Gitlab-Token") String gitlabToken,
            @RequestBody(required = false) JsonNode json) throws AppException {
        String effectiveToken = token != null ? token : gitlabToken;
        if (effectiveToken == null) {
            throw new NotValidException("Token not found in headers");
        }
        AccessToken at = accessTokenService.getByToken(effectiveToken);
        Project p = projectService.get(projectId);
        projectService.update(at, p, json);
        return MResponse.ok("Update proccess stated");
    }

    @PostMapping("/project")
    public MProject create(@RequestHeader String token,
            @RequestBody MProject mp) throws AppException {
        accessTokenService.getAdminToken(token);
        Project p = projectService.create(mp);
        return projectService.db2json(p);
    }

    @PostMapping("/project/{projectId}")
    public MProject edit(@PathVariable String projectId,
            @RequestHeader String token,
            @RequestBody MProject mp) throws AppException {
        accessTokenService.getAdminToken(token);
        Project p = projectService.get(projectId);
        projectService.edit(p, mp);
        return projectService.db2json(p);
    }

    @DeleteMapping("/project/{projectId}")
    public MResponse delete(@PathVariable String projectId,
            @RequestHeader String token) throws AppException {
        accessTokenService.getAdminToken(token);
        Project p = projectService.get(projectId);
        projectService.delete(p);
        return MResponse.ok("Project deleted");
    }

}
